# SliceUp!
![icon](SliceUp/Assets.xcassets/AppIcon.appiconset/icon@2x.png)

#### iOS app for help in slicing products
It was developed in "foodhack.partiyaedi" hackaton

##### Technology stack
* CoreML for detecting product type
* OpenCV for detecting circle center and radius
* ARKit and SceneKit for detecting table plane and drawing 3d model with slicing

##### Sample with pizza
![Sample with pizza](sliceup-pizza.gif)