//
//  CutType.swift
//  SliceUp
//
//  Created by Pavel Chupryna on 04/03/2018.
//  Copyright © 2018 Suchapega. All rights reserved.
//

import Foundation

enum CutType {
    case radial
    case line
    case grid
}
