//
//  CuttableProtocol.swift
//  SliceUp
//
//  Created by Pavel Chupryna on 03/03/2018.
//  Copyright © 2018 Suchapega. All rights reserved.
//

import Foundation
import SceneKit

protocol CuttableProtocol {
    var pieces: Int { get }
    var cutLines: [SCNNode] { get }
    func cutFor(persons: Int, withType: CutType)
    func clear()
}
