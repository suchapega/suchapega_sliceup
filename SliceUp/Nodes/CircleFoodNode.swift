//
//  CircleFoodNode.swift
//  SliceUp
//
//  Created by Pavel Chupryna on 03/03/2018.
//  Copyright © 2018 Suchapega. All rights reserved.
//

import UIKit
import SceneKit

class CircleFoodNode: SCNNode, CuttableProtocol {
    
    required override init() {
        self.cylinder = SCNCylinder()
        self.pieces = 0
        self.cutLines = []
        
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.cylinder = SCNCylinder()
        self.pieces = 0
        self.cutLines = []
        
        super.init(coder: aDecoder)
    }
    
    required convenience init(radius: CGFloat) {
        self.init()
        
        cylinder = SCNCylinder(radius: radius, height: 0.005)
        cylinder.radialSegmentCount = Int(cylinder.radius * .pi * 2 * 100)
        cylinder.firstMaterial?.transparency = 0.25
        super.geometry = cylinder
    }
    
    let cutLineWidth: CGFloat = 0.0005
    let cutLineStands: CGFloat = 0.0001
    var cylinder: SCNCylinder
    
    var pieces: Int
    var cutLines: [SCNNode]
    
    func clear() {
        for cutLine in cutLines {
            cutLine.removeFromParentNode()
        }
        cutLines = []
        self.pieces = 0
        
        self.removeFromParentNode()
    }
    
    func cutFor(persons: Int, withType cutType: CutType) {
        switch cutType {
        case .radial:
            cutRadialTypeFor(persons: persons)
        case .line:
            cutLineTypeFor(persons: persons)
        case .grid:
            cutGridTypeFor(persons: persons)
        default:
            return
        }
    }
    
    func cutRadialTypeFor(persons: Int) {
        for cutLine in cutLines {
            cutLine.removeFromParentNode()
        }
        cutLines = []
        
        //todo еще добавить расчет количества относительно размера
        pieces = persons
        
        let pieceAngle = .pi * 2 / Float(pieces)
        var stepAngle: Float = 0.0
        for piece in 0..<pieces {
            
            let lineBox = SCNBox(width: cutLineWidth, height: self.cylinder.height + cutLineStands, length: self.cylinder.radius + cutLineWidth + cutLineStands, chamferRadius: 0)
            lineBox.firstMaterial?.isDoubleSided = true
            lineBox.firstMaterial?.diffuse.contents = UIColor.black
            //            lineBox.firstMaterial?.transparency = 0.5
            
            let lineNode = SCNNode(geometry: lineBox)
            let correction = Float(self.cylinder.radius / 2)
            lineNode.position = SCNVector3Make(self.position.x + correction * sin(stepAngle), self.position.y, self.position.z + correction * cos(stepAngle))
            lineNode.eulerAngles = SCNVector3Make(0, stepAngle, 0)
            
            cutLines.append(lineNode)
            self.addChildNode(lineNode)
            
            stepAngle += pieceAngle
        }
        
        let centerSphere = SCNCapsule(capRadius: cutLineWidth, height: self.cylinder.height + cutLineStands)
        centerSphere.firstMaterial?.isDoubleSided = true
        centerSphere.firstMaterial?.diffuse.contents = UIColor.black
        let centerNode = SCNNode(geometry: centerSphere)
        centerNode.position = self.position
        
        cutLines.append(centerNode)
        self.addChildNode(centerNode)
    }
    
    func cutLineTypeFor(persons: Int) {
        
    }
    
    func cutGridTypeFor(persons: Int) {
        
        for cutLine in cutLines {
            cutLine.removeFromParentNode()
        }
        cutLines = []
        
        //todo еще добавить расчет количества относительно размера
        pieces = persons
        
        if persons <= 4
        {
            cutRadialTypeFor(persons:persons)
        }
        else
            if pieces > 4 && pieces < 9
        {
            
            var param = 1.0
            
            for i in 0..<2 {
            
                let lineBox = SCNBox(width: cutLineWidth, height: self.cylinder.height + cutLineStands, length: 2 * self.cylinder.radius, chamferRadius: 0)
                lineBox.firstMaterial?.isDoubleSided = true
                lineBox.firstMaterial?.diffuse.contents = UIColor.black
                
                if i % 2 != 0
                {
                    param = -1.0
                }
                else
                {
                    param = 1.0
                }
                
                let lineNode = SCNNode(geometry: lineBox)
                lineNode.position = SCNVector3Make(self.position.x + Float(param) * Float(self.cylinder.radius/3), self.position.y, self.position.z)
                lineNode.eulerAngles = SCNVector3Make(0, 0, 0)
                
                cutLines.append(lineNode)
                self.addChildNode(lineNode)
                
            }
            
            for i in 0..<2 {
                
                let lineBox = SCNBox(width: cutLineWidth, height: self.cylinder.height + cutLineStands, length: 2 * self.cylinder.radius, chamferRadius: 0)
                lineBox.firstMaterial?.isDoubleSided = true
                lineBox.firstMaterial?.diffuse.contents = UIColor.black
                
                if i % 2 != 0
                {
                    param = -1.0
                }
                else
                {
                    param = 1.0
                }
                
                let lineNode = SCNNode(geometry: lineBox)
                lineNode.position = SCNVector3Make(self.position.x, self.position.y, self.position.z + Float(param) * Float(self.cylinder.radius/3))
                lineNode.eulerAngles = SCNVector3Make(0, .pi/2, 0)
                
                cutLines.append(lineNode)
                self.addChildNode(lineNode)
                
            }
            
            pieces = 9
            
        }
        else{
            
            var i = -2
            
            while i <= 2 {
                
                let lineBox = SCNBox(width: cutLineWidth, height: self.cylinder.height + cutLineStands, length: 2 * self.cylinder.radius, chamferRadius: 0)
                lineBox.firstMaterial?.isDoubleSided = true
                lineBox.firstMaterial?.diffuse.contents = UIColor.black
                
               
                let lineNode = SCNNode(geometry: lineBox)
                lineNode.position = SCNVector3Make(self.position.x + Float(i) * Float(self.cylinder.radius / 4), self.position.y, self.position.z)
                lineNode.eulerAngles = SCNVector3Make(0, 0, 0)
                
                cutLines.append(lineNode)
                self.addChildNode(lineNode)
                
                 i+=2
            }
            
            i = -2
            
            while i <= 2 {
                
                let lineBox = SCNBox(width: cutLineWidth, height: self.cylinder.height + cutLineStands, length: 2 * self.cylinder.radius, chamferRadius: 0)
                lineBox.firstMaterial?.isDoubleSided = true
                lineBox.firstMaterial?.diffuse.contents = UIColor.black
                
                
                let lineNode = SCNNode(geometry: lineBox)
                lineNode.position = SCNVector3Make(self.position.x, self.position.y, self.position.z + Float(i) * Float(self.cylinder.radius / 4))
                lineNode.eulerAngles = SCNVector3Make(0, .pi/2, 0)
                
                cutLines.append(lineNode)
                self.addChildNode(lineNode)
                
                i+=2
            }
                
            pieces = 16
        }
       
    }
}

