//
//  OpenCVWrapper.m
//  food_hack
//
//  Created by Sofia Romanova on 03/03/2018.
//  Copyright © 2018 Sofia Romanova. All rights reserved.
//

#import "OpenCVWrapper.h"
#import <opencv2/opencv.hpp>
#import <opencv2/imgcodecs/ios.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;

@implementation OpenCVWrapper
-(double *) pizzaRecognition:(CVPixelBufferRef) pixelBuffer
    {
        //X center
        //Y center
        //radius
        double static returnedData[3]  = {0.0,0.0,0.0};
        
        Mat src, src_gray;
        
        cv::Mat mat;
        CVPixelBufferLockBaseAddress(pixelBuffer, 0);
        //Get only the data from the plane we're interested in
        void *address =  CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 0);
        int bufferWidth = (int)CVPixelBufferGetWidthOfPlane(pixelBuffer,0);
        int bufferHeight = (int)CVPixelBufferGetHeightOfPlane(pixelBuffer, 0);
        int bytePerRow = (int)CVPixelBufferGetBytesPerRowOfPlane(pixelBuffer, 0);
        Mat src_t = cv::Mat(bufferHeight, bufferWidth, CV_8UC1, address, bytePerRow).clone();
        
        cv::transpose(src_t, src);
//        cv::flip(src, src,1);
        
        
        if( !src.data )
        {
            CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
            return returnedData;
        }

        /// Convert it to gray
        //cvtColor( src, src_gray, CV_BGR2GRAY );

        // Reduce the noise so we avoid false circle detection
        GaussianBlur( src, src_gray, cv::Size(9, 9), 2, 2 );

        vector<Vec3f> circles;
        
        /// Apply the Hough Transform to find the circles
        HoughCircles( src_gray, circles, CV_HOUGH_GRADIENT, 1 , 500);
        
        
        double maxRadius = -100.0;
        int elemNumber = -1;
        
        /// Draw the circles detected
        for( size_t i = 0; i < circles.size(); ++i )
        {
            //cv::Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
            double currentRadius = cvRound(circles[i][2]);
            
            if(currentRadius > maxRadius)
            {
                maxRadius = currentRadius;
                elemNumber = i;
            }
            
            
            // circle center
            //circle( src, center, 3, Scalar(0,255,0), -1, 8, 0 );
            // circle outline
            //circle( src, center, radius, Scalar(0,0,255), 3, 8, 0 );
        }
        
        if(elemNumber!= -1)
        {
            returnedData[0] = cvRound(circles[elemNumber][0]);
            returnedData[1] = cvRound(circles[elemNumber][1]);
            returnedData[2] = maxRadius;
        }
        
        //UIImage* returnedImage = MatToUIImage(src);
        
        CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
        return returnedData;
        
        //return returnedImage;
    }
@end
