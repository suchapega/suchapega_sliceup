//
//  OpenCVWrapper.h
//  food_hack
//
//  Created by Sofia Romanova on 03/03/2018.
//  Copyright © 2018 Sofia Romanova. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OpenCVWrapper : NSObject
-(double *) pizzaRecognition:(CVPixelBufferRef)pixelBuffer;
@end
