//
//  ViewController.swift
//  SliceUp
//
//  Created by Pavel Chupryna on 03/03/2018.
//  Copyright © 2018 Suchapega. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import CoreML

import Vision

class ViewController: UIViewController, ARSCNViewDelegate, UICollectionViewDataSource {

    let openCV: OpenCVWrapper
    let dispatchQueueOpenCV = DispatchQueue(label: "com.suchapega.opencv")
    
    var visionRequests = [VNRequest]()
    let dispatchQueueML = DispatchQueue(label: "com.suchapega.ml")
    
    var cuttableObject: CuttableProtocol?
    
    var detectedCircle: [Double]?
    var detectedObject: String?
    
    var selectedCutType: CutType
    var selectedPersonsNumber: Int
    var persons = Array(2...14)
    
    @IBOutlet weak var sceneView: ARSCNView!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var piecesLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var slicingRadialButton: UIButton!
    @IBOutlet weak var slicingLineButton: UIButton!
    @IBOutlet weak var slicingGridButton: UIButton!
    @IBOutlet weak var personsView: UIView!
    @IBOutlet weak var personsCollectionView: UIPersonsCollectionView!
    
    required init?(coder aDecoder: NSCoder) {
        self.openCV = OpenCVWrapper()
        
        selectedCutType = .radial
        selectedPersonsNumber = 2
        
        super.init(coder: aDecoder)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        
        sceneView.delegate = self
        sceneView.autoenablesDefaultLighting = true
        
        sceneView.scene = SCNScene()
        
//        sceneView.showsStatistics = true
//        sceneView.antialiasingMode = .multisampling4X
//        sceneView.debugOptions = ARSCNDebugOptions.showFeaturePoints
        
        
        // Set up Vision Model
        guard let selectedModel = try? VNCoreMLModel(for: /*Food101*/ Inceptionv3().model) else {
            fatalError("Could not load model")
        }
        
        // Set up Vision-CoreML Request
        let classificationRequest = VNCoreMLRequest(model: selectedModel, completionHandler: classificationCompleteHandler)
        classificationRequest.imageCropAndScaleOption = VNImageCropAndScaleOption.centerCrop // Crop from centre of images and scale to appropriate size.
        visionRequests = [classificationRequest]
        
        // Begin Loop to Update CoreML
        loopCoreMLUpdate()
        
        
    }
    
    func initViews() {
        let topGradient = CAGradientLayer()
        topGradient.frame = topView.bounds
        topGradient.colors = [UIColor.black.withAlphaComponent(0.3).cgColor, UIColor.clear.cgColor]
        topView.layer.insertSublayer(topGradient, at: 0)
        
        let bottomGradient = CAGradientLayer()
        bottomGradient.frame = bottomView.bounds
        bottomGradient.colors = [UIColor.clear.cgColor, UIColor.black.withAlphaComponent(0.3).cgColor]
        bottomView.layer.insertSublayer(bottomGradient, at: 0)
        
        slicingRadialButton.layer.cornerRadius = slicingRadialButton.bounds.height / 2
        slicingLineButton.layer.cornerRadius = slicingLineButton.bounds.height / 2
        slicingGridButton.layer.cornerRadius = slicingGridButton.bounds.height / 2
        personsView.layer.cornerRadius = personsView.bounds.height / 2
        personsCollectionView.layer.cornerRadius = personsCollectionView.bounds.height / 2
        
        slicingRadialButton.isSelected = true
        slicingLineButton.isEnabled = false
        
        let personsLayout = UIPersonsFlowLayout()
        personsLayout.itemSize = CGSize(width: 40, height: 48)
        personsLayout.minimumLineSpacing = 0
        personsLayout.minimumLineSpacing = 0
        personsLayout.scrollDirection = .horizontal
        personsCollectionView.collectionViewLayout = personsLayout
        
        personsCollectionView.register(UIPersonCollectionViewCell.self, forCellWithReuseIdentifier: UIPersonCollectionViewCell.key)
        personsCollectionView.dataSource = self
        personsCollectionView.reloadData()
        
//        var insets = personsCollectionView.contentInset
//        let value = (personsCollectionView.bounds.width - personsLayout.itemSize.width) / 2
//        insets.left = value
//        insets.right = value
//        personsCollectionView.contentInset = insets
//        personsCollectionView.decelerationRate = UIScrollViewDecelerationRateFast;
        
        personsCollectionView.delegate = personsCollectionView
        personsCollectionView.onPersonsChanged = self.onPersonChanged
    }
    
    func onPersonChanged(personsIndex: Int) {
        if personsIndex >= 0 && personsIndex < persons.count {
            self.selectedPersonsNumber = persons[personsIndex]
            self.cut()
        }
        
//        var indexesForUpdate = self.personsCollectionViewindexPathsForVisibleItems
//        indexesForUpdate.append(IndexPath(row: currentPage, section: 0))
//        self.personsCollectionView.reloadItems(at: indexesForUpdate)
        
        self.personsCollectionView.reloadItems(at: [IndexPath(row: personsIndex, section: 0)])
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return persons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UIPersonCollectionViewCell.key, for: indexPath)
        
        if let cell = cell as? UIPersonCollectionViewCell {
            if indexPath.row >= 0 && indexPath.row < persons.count {
                cell.personsLabel.text = String(persons[indexPath.row])
                cell.changeType(.notSelected)
                if indexPath.row == persons.index(of: self.selectedPersonsNumber) {
                    cell.changeType(.selected)
                }
            }
        }
        
        return cell
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//
//        self.detectedCircle = [0.0, 0.0, 0.0]
//        let location = touches.first!.location(in: sceneView)
//        let hitResultsFeaturePoints: [ARHitTestResult] = sceneView.hitTest(location, types: .featurePoint)
//        if let hit = hitResultsFeaturePoints.first {
//            sceneView.session.add(anchor: ARAnchor(transform: hit.worldTransform))
//        }
//    }
    
    @IBAction func onRefreshButton() {
        if let cuttableObject = cuttableObject {
            cuttableObject.clear()
            self.cuttableObject = nil
            self.detectedCircle = nil
        }
        
        self.titleLabel.text = "Наведите на продукт"
        
        circleFinding()
        loopCoreMLUpdate()
    }
    
    @IBAction func onSlicingRadialButton() {
        clearAllCutTypeButtons()
        slicingRadialButton.isSelected = true
        selectedCutType = .radial
        cut()
    }
    
    @IBAction func onSlicingLineButton() {
        clearAllCutTypeButtons()
        slicingLineButton.isSelected = true
        selectedCutType = .line
        cut()
    }
    
    @IBAction func onSlicingGridButton() {
        clearAllCutTypeButtons()
        slicingGridButton.isSelected = true
        selectedCutType = .grid
        cut()
    }
    
    func clearAllCutTypeButtons() {
        slicingRadialButton.isSelected = false
        slicingLineButton.isSelected = false
        slicingGridButton.isSelected = false
    }
    
    
    func cut() {
        self.cuttableObject?.cutFor(persons: self.selectedPersonsNumber, withType: self.selectedCutType)
        DispatchQueue.main.async {
            if let cuttableObject = self.cuttableObject {
                self.piecesLabel.text = String(cuttableObject.pieces)
            } else {
                self.piecesLabel.text = "—"
            }
        }
    }
    
    
    func circleFinding() {
        dispatchQueueOpenCV.async {
            self.tryToFindCircle()
            
            if self.detectedCircle == nil {
                self.circleFinding()
            }
        }
    }
    
    func tryToFindCircle() {
        let pixbuff: CVPixelBuffer? = (self.sceneView.session.currentFrame?.capturedImage)
        if pixbuff == nil { return }
        
        let resData = self.openCV.pizzaRecognition(pixbuff)
        if resData![2] > 0.0 {
            self.detectedCircle = [resData![0], resData![1], resData![2]]
            addCuttableNode(ofCircle: self.detectedCircle!)
            print("Circle detected: \(self.detectedCircle)")
        }
    }
    
    func addCuttableNode(ofCircle circle: [Double]) {
        let center = CGPoint(x: CGFloat(circle[0]) / sceneView.contentScaleFactor, y: CGFloat(circle[1]) / sceneView.contentScaleFactor)
        let centerFP: [ARHitTestResult] = sceneView.hitTest(center, types: .featurePoint)
        if let featurePoint = centerFP.first {
            sceneView.session.add(anchor: ARAnchor(transform: featurePoint.worldTransform))
        }
    }
    
    
    
    
    // MARK: - ARSCNViewDelegate
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if anchor.isKind(of: ARPlaneAnchor.self) && self.detectedCircle == nil {
            circleFinding()
        } else if self.detectedCircle != nil && self.cuttableObject == nil {
            DispatchQueue.main.async {
                
//                let edge = CGPoint(x: CGFloat(self.detectedCircle![0]) / self.sceneView.contentScaleFactor, y: CGFloat(self.detectedCircle![1] + self.detectedCircle![2]) / self.sceneView.contentScaleFactor)
//                let edgeFP: [ARHitTestResult] = self.sceneView.hitTest(edge, types: .featurePoint)
//
//                if let featurePoint = edgeFP.first {
//                    let edgeAnchor = ARAnchor(transform: featurePoint.worldTransform)
//
//                    let vector = SCNVector3Make(edgeAnchor. // .position.x - endNode.position.x, startNode.position.y - endNode.position.y, startNode.position.z - endNode.position.z)
//                    let realRadius = sqrtf(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z)
//
                    let model = CircleFoodNode(radius: 0.125)//realRadius)
                    model.position = SCNVector3Zero
                    node.addChildNode(model)
                    
                    self.cuttableObject = model
                    self.cut()
//                }
            }
        }
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
    }
    
    func classificationCompleteHandler(request: VNRequest, error: Error?) {
        // Catch Errors
        if error != nil {
            print("Error: " + (error?.localizedDescription)!)
            return
        }
        guard let observations = request.results else {
            print("No results")
            return
        }
        
        // Get Classifications
        let classifications = observations[0...1] // top 2 results
            .flatMap({ $0 as? VNClassificationObservation })
            .map({ "\($0.identifier) \(String(format:"- %.2f", $0.confidence))" })
            .joined(separator: "\n")
        
        DispatchQueue.main.async {
//            // Print Classifications
//            print(classifications)
//            print("--")
//
//            // Display Debug Text on screen
//            var debugText:String = ""
//            debugText += classifications
//            print(debugText)
            
            // Store the latest prediction
            var objectName:String = "…"
            objectName = classifications.components(separatedBy: "-")[0]
            objectName = objectName.components(separatedBy: ",")[0]
            
            self.detectedObject = objectName
            self.setRussianTitle(objectName)
//            print("Object detected: \(self.detectedObject)")
        }
    }
    
    func setRussianTitle(_ foodType: String) {
        DispatchQueue.main.async {
            if foodType.contains("pizza") {
                self.titleLabel.text = "Пицца"
            } else if foodType.contains("pie") {
                self.titleLabel.text = "Пирог"
            } else if foodType.contains("cake") {
                self.titleLabel.text = "Торт"
            } else if foodType.contains("donut") {
                self.titleLabel.text = "Пончик"
            } else if foodType.contains("meat") {
                self.titleLabel.text = "Мясо"
            }
//            else {
//                self.titleLabel.text = foodType
//            }
        }
    }
    
    func loopCoreMLUpdate() {
        dispatchQueueML.async {
            self.updateCoreML()
            
            if self.cuttableObject == nil {
                self.loopCoreMLUpdate()
            }
        }
        
    }
    
    func updateCoreML() {
        ///////////////////////////
        // Get Camera Image as RGB
        let pixbuff : CVPixelBuffer? = (sceneView.session.currentFrame?.capturedImage)
        if pixbuff == nil { return }
        let ciImage = CIImage(cvPixelBuffer: pixbuff!)
        // Note: Not entirely sure if the ciImage is being interpreted as RGB, but for now it works with the Inception model.
        // Note2: Also uncertain if the pixelBuffer should be rotated before handing off to Vision (VNImageRequestHandler) - regardless, for now, it still works well with the Inception model.
        
        ///////////////////////////
        // Prepare CoreML/Vision Request
        let imageRequestHandler = VNImageRequestHandler(ciImage: ciImage, options: [:])
        // let imageRequestHandler = VNImageRequestHandler(cgImage: cgImage!, orientation: myOrientation, options: [:]) // Alternatively; we can convert the above to an RGB CGImage and use that. Also UIInterfaceOrientation can inform orientation values.
        
        ///////////////////////////
        // Run Image Request
        do {
            try imageRequestHandler.perform(self.visionRequests)
        } catch {
            print(error)
        }
        
    }
  
}

