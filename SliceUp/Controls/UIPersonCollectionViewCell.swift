//
//  UIPersonCollectionViewCell.swift
//  SliceUp
//
//  Created by Pavel Chupryna on 04/03/2018.
//  Copyright © 2018 Suchapega. All rights reserved.
//

import UIKit

class UIPersonCollectionViewCell: UICollectionViewCell {
    
    static let key = "PersonCell"
    
    var personsLabel: UILabel
    
    override init(frame: CGRect) {
        personsLabel = UILabel()
        super.init(frame: frame)
        
        contentView.addSubview(personsLabel)
        personsLabel.translatesAutoresizingMaskIntoConstraints = false
        personsLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        personsLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        
        personsLabel.textAlignment = .center
    }
    
    required init?(coder aDecoder: NSCoder) {
        personsLabel = UILabel()
        super.init(coder: aDecoder)
    }
    
    func changeType(_ type: PersonsCellType) {
        switch type {
        case .selected:
            personsLabel.font = UIFont.systemFont(ofSize: 26)
            personsLabel.textColor = UIColor(rgb: 0xB41226)
        default:
            personsLabel.font = UIFont.systemFont(ofSize: 20)
            personsLabel.textColor = UIColor(rgb: 0x717171)
        }
    }
}

enum PersonsCellType {
    case selected
    case notSelected
}

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }
    
    convenience init(rgb: Int, a: CGFloat = 1.0) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF,
            a: a
        )
    }
}
