//
//  UIPersonsCollectionView.swift
//  SliceUp
//
//  Created by Pavel Chupryna on 04/03/2018.
//  Copyright © 2018 Suchapega. All rights reserved.
//

import UIKit

class UIPersonsCollectionView: UICollectionView, UICollectionViewDelegate {
    
    var onPersonsChanged: (Int) -> Void
    
    required init?(coder aDecoder: NSCoder) {
        onPersonsChanged = { $0 }
        super.init(coder: aDecoder)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
//        let w = self.bounds.width - (self.contentInset.left + self.contentInset.right)
        let w = self.bounds.width / 3
        let currentPage = Int(ceil(x / w))
        
        onPersonsChanged(currentPage)
    }

}
